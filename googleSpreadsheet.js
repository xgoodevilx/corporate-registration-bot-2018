const google = require('googleapis');
const privatekey = require('./privatekey.json');
const moment = require('moment');
const Rx = require('rxjs/Rx');

const jwtClient = new google.auth.JWT(
    privatekey.client_email,
    null,
    privatekey.private_key,
    ['https://www.googleapis.com/auth/spreadsheets']);


const spreadsheetId = 'spreadsheetId';

const service = google.sheets('v4');
const connections = 'Connections!A:G';
const feedbacks = 'Feedbacks!A:C';
const votes = 'Votes!A:D';
const groups = 'Groups!A:B';
const userslist = 'Userslist!A:B';

let teamsArr = [];

module.exports = {
    addConnection: function (chatId, username, firstname, lastname) {
        jwtClient.authorize();
        service.spreadsheets.values.append({
            auth: jwtClient,
            spreadsheetId: spreadsheetId,
            range: connections,
            valueInputOption: 'RAW',
            resource: {values: [[chatId, username, firstname, lastname, moment().format('DD-MM-YYYY HH:mm:ss')]]}
        }, (err, result) => {
            if (err) {
                // Handle error
                console.log(err);
            }
        });
    },


     findNameInConnections: function () {
			let connectionsArr = [];
			
				jwtClient.authorize();
					return Rx.Observable.create(observer => {
					
					service.spreadsheets.values.get({
						auth: jwtClient,
						spreadsheetId: spreadsheetId,
						range: connections
					}, (err, response) => {
						if (err) {
							console.log('The API returned an error: ' + err);
						} else {
							response.values.shift();
							response.values.forEach(row => {
								 let cell = '' +row[6] ;
								connectionsArr[6] = cell;
										for (let index = 0; index < row.length; ++index) {
													let cell = '' +row[index] ;
													connectionsArr[index] = cell;
										}
			
										 observer.next(connectionsArr);
										return connectionsArr;	
										observer.complete();
									 return connectionsArr;
									 observer.complete();
								
				
                    })
                }
            });
			
        });
    },
 
	findNameSurnameInCompareUsersList: function () {
			let teamsArr = [];
	
        jwtClient.authorize();
			return Rx.Observable.create(observer => {
			
            service.spreadsheets.values.get({
                auth: jwtClient,
                spreadsheetId: spreadsheetId,
                range: userslist
            }, (err, response) => {
                if (err) {
                    console.log('The API returned an error: ' + err);
                } else {
                    response.values.shift();
                    response.values.forEach(row => {
								for (let index = 0; index < row.length; ++index) {
											let cell = '' +row[index] ;
											teamsArr[index] = cell;
								}
	
								observer.next(teamsArr);
								return teamsArr;	
								observer.complete();
                    })
                }
            });
			
        });

    },


    getChatId: function (username) {
        jwtClient.authorize();
        return Rx.Observable.create(observer => {
            service.spreadsheets.values.get({
                auth: jwtClient,
                spreadsheetId: spreadsheetId,
                range: connections
            }, (err, response) => {
                if (err) {
                    console.log('The API returned an error: ' + err);
                } else {
                    response.values.shift();
                    response.values.forEach(row => {
                        if (row[1] === username) {
                            observer.next(Number(row[0]));
                            observer.complete();
                        }
                    })
                }
            });
        });
    },
    getChats: function () {
        jwtClient.authorize();
        return Rx.Observable.create(observer => {
            service.spreadsheets.values.get({
                auth: jwtClient,
                spreadsheetId: spreadsheetId,
                range: connections
            }, (err, response) => {
                if (err) {
                    console.log('The API returned an error: ' + err);
                } else {
                    response.values.shift();
                    observer.next(response.values.map(i => {
                        return {chatId: Number(i[0]), username: i[1], firstname: i[2], lastname: i[3], name: i[5], direction: i[6]}
                    }));
                    observer.complete();
                }
            });
        });
    },

    addFeedback: function (username, text) {
        jwtClient.authorize();
        service.spreadsheets.values.append({
            auth: jwtClient,
            spreadsheetId: spreadsheetId,
            range: feedbacks,
            valueInputOption: 'RAW',
            resource: {values: [[moment().format('DD-MM-YYYY HH:mm:ss'), username, text]]}
        }, (err, result) => {
            if (err) {
                // Handle error
                console.log(err);
            }
        });
    },

    addGroupChat: function (chatId) {
        jwtClient.authorize();
        service.spreadsheets.values.append({
            auth: jwtClient,
            spreadsheetId: spreadsheetId,
            range: groups,
            valueInputOption: 'RAW',
            resource: {values: [[chatId, false]]}
        }, (err, result) => {
            if (err) {
                // Handle error
                console.log(err);
            }
        });
    },

    getAdminChatId: function () {
        jwtClient.authorize();
        return Rx.Observable.create(observer => {
            service.spreadsheets.values.get({
                auth: jwtClient,
                spreadsheetId: spreadsheetId,
                range: groups
            }, (err, response) => {
                if (err) {
                    console.log('The API returned an error: ' + err);
                } else {
                    response.values.shift();
                    response.values.forEach(row => {
                        if (Boolean(row[1])) {
                            observer.next(row[0]);
                            observer.complete();
							
                        }
                    })
                }
            });
        });
    },

    addVote: function (username, question, answer) {
        jwtClient.authorize();
        service.spreadsheets.values.append({
            auth: jwtClient,
            spreadsheetId: spreadsheetId,
            range: votes,
            valueInputOption: 'RAW',
            resource: {values: [[moment().format('DD-MM-YYYY HH:mm:ss'), username, question, answer]]}
        }, (err, result) => {
            if (err) {
                // Handle error
                console.log(err);
            }
        });
    },

    choiceName: function (chatId, name) {
        module.exports.getChats().subscribe(chats => {
            let i = 2;
            chats.forEach(chat => {
                if (chat.chatId === chatId) {
                    jwtClient.authorize();
                    service.spreadsheets.values.update({
                        auth: jwtClient,
                        spreadsheetId: spreadsheetId,
                        range: `Connections!F${i}`,
                        valueInputOption: 'RAW',
                        resource: {values: [[name]]}
                    }, (err, result) => {
                        if (err) {
                            // Handle error
                            console.log(err);
                        }
                    });
                }
                i++;
            })
        });
    },

    choiceDirection: function (chatId, name) {
        module.exports.getChats().subscribe(chats => {
            let i = 2;
            chats.forEach(chat => {
                if (chat.chatId === chatId) {
                    jwtClient.authorize();
                    service.spreadsheets.values.update({
                        auth: jwtClient,
                        spreadsheetId: spreadsheetId,
                        range: `Connections!G${i}`,
                        valueInputOption: 'RAW',
                        resource: {values: [[name]]}
                    }, (err, result) => {
                        if (err) {
                            // Handle error
                            console.log(err);
                        }
                    });
                }
                i++;
            })
        });
    }
};
