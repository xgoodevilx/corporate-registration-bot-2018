const googleSpreadsheet = require('./googleSpreadsheet');
const TelegramBot = require('node-telegram-bot-api');
/* 

/sendall  
/send_main  stream
/send_tech  stream
/send_work  stream

*/

const tokenClient = 'tokenClient'; 
const util = require('util');
const bot = new TelegramBot(tokenClient, {polling: true});

const FEEDBACK_TEXT = `Мы будем рады Вашей обратной связи о ____Party!`;
const NAME_TEXT = `Напишите, пожалуйста, Ваше имя и фамилию 😊`;
const textEveningEvents = `В течении вечера для Вас будут доступны интерактивные развлечения:
		-
		-
		-
		-
	Участвуйте в активностях и получайте призы от партнеров за лучшие результаты. `;


let adminChatId;

googleSpreadsheet.getAdminChatId().subscribe(chatId => adminChatId = Number(chatId));


function nospace(str) { 
var VRegExp = new RegExp(/\s+/g); 
var VResult = str.replace(VRegExp, ''); 
return VResult.toLowerCase()
}


bot.onText(/\/start/, (msg) => {
    if (msg.chat.id === adminChatId) return;
    const username = getUsername(msg.chat);
    const inline_keyboard = {};
	
    const options = {parse_mode: 'HTML', reply_markup: inline_keyboard};
	
    bot.sendMessage(msg.chat.id, `Добро пожаловать на ___ Party! 🙌
`, options);

    googleSpreadsheet.getChats().subscribe(chats => {
        if (!chats.find(i => i.chatId === msg.chat.id)) {
            googleSpreadsheet.addConnection(msg.chat.id, username, msg.chat.first_name, msg.chat.last_name);
        }

		// Enter name surname
		 bot.sendMessage(msg.chat.id, NAME_TEXT,  ({reply_markup: {force_reply: true}}));

    });
	
});

// Оставить отзыв

bot.onText(/📮 Оставить отзыв/, (msg) => {
    const chatId = msg.chat.id;
    if (chatId === adminChatId) return;
    bot.sendMessage(chatId, "✍ Мы будем рады, Вашей обратной связи о ___ Party!", {reply_markup: {force_reply: true}});
});

bot.onText(/Меню/, (msg) => {
    const chatId = msg.chat.id;
    if (chatId === adminChatId) return;
    sendMenu(chatId);
});

bot.onText(/К списку развлечений/, (msg) => {
    const chatId = msg.chat.id;
    if (chatId === adminChatId) return;
    sendMenuBackBtnOption(chatId);
});

bot.on('message', (msg) => {
    if (msg.entities) return;
    const chatId = msg.chat.id;
    const username = getUsername(msg.chat);

// Меню
    if (msg.chat.id !== adminChatId && msg.text !== 'Меню') {
        if (msg.reply_to_message && msg.reply_to_message.text === FEEDBACK_TEXT) {
            const resp = msg.text;
            googleSpreadsheet.addFeedback(username, resp);
            bot.sendMessage(adminChatId, `New feedback from ${username}: ${resp}`);
//feedback text
            bot.sendMessage(chatId, `Спасибо за Ваш отзыв! Мы рады, что Вы провели ____ Party  вмести с нами. До встречи на новых мероприятиях ____ ❤`, menuBtnOption());
            sendMenu(chatId);
        }
        else if (msg.reply_to_message && msg.reply_to_message.text === NAME_TEXT) {
            let name = msg.text;
            let option = menuBtnOption();
            option.parse_mode = 'HTML';
			  googleSpreadsheet.findNameSurnameInCompareUsersList().subscribe( nameSurnameInCompareUsersList => {
					 if (nospace(nameSurnameInCompareUsersList[0]) == nospace(name) ) {
							console.log(nameSurnameInCompareUsersList[1] + ' group  ' );
							googleSpreadsheet.choiceDirection(chatId, nameSurnameInCompareUsersList[1]);

							bot.sendMessage(chatId, `Приятно познакомиться <strong>${nameSurnameInCompareUsersList[0]}</strong>`, option);
							bot.sendMessage(chatId, `Вы в комманде <strong>${nameSurnameInCompareUsersList[1]}</strong>`, option);
							googleSpreadsheet.choiceName(chatId, nameSurnameInCompareUsersList[0] );
							sendMenu(chatId);
							bot.sendMessage(adminChatId, 'Зарегистрирован - ' + nameSurnameInCompareUsersList[0] + ' в группе - ' + nameSurnameInCompareUsersList[1] + ' !  /send_'+ nameSurnameInCompareUsersList[1] );

					}

				} ) ;

						googleSpreadsheet.findNameInConnections().subscribe( nameInConnections => {
									if (nameInConnections[0] == chatId  ) {
										 if ( nameInConnections[5] == ''  ) {
												setTimeout(() => {
														bot.sendMessage(chatId, NAME_TEXT, ({reply_markup: {force_reply: true}}));
												}, 0);
										}			
									}
					   } ) ;						 
    }
        else {
            bot.forwardMessage(adminChatId, chatId, msg.message_id);
            sendQuestionReceived(chatId, msg.text);
            sendMenu(chatId);
        }
    } else if (msg.reply_to_message && msg.reply_to_message.forward_from) {
        const origUsername = getUsername(msg.reply_to_message.forward_from);
        googleSpreadsheet.getChatId(origUsername).subscribe(origChatId => {
            if (msg.photo) {
                bot.sendPhoto(origChatId, msg.photo[0].file_id, {caption: msg.caption});
            } else if (msg.sticker) {
                bot.sendSticker(origChatId, msg.sticker.file_id);
            } else if (msg.text) {
                bot.sendMessage(origChatId, msg.text)
            }
        })
    } else if (msg.group_chat_created) {
        googleSpreadsheet.addGroupChat(chatId);
    }
});

bot.on('callback_query', (msg) => {
    const username = getUsername(msg.message.chat);
    const text = msg.message.text;
    const callback = msg.data;
    const chatId = msg.message.chat.id;
	            let option = menuBtnOption();
				option.parse_mode = 'HTML';

    switch (callback) {
        case 'ignore':
            return;
        case 'here':
            sendName(msg.message);
            break;
        case 'leave_name':
            nameReply(msg.message);
            break;
        case 'edit_name':
            nameReply2(msg.message);
            break;
        case 'question':
            sendQuestion(msg.message);
            break;
        case 'direction':
            sendDirection(msg.message);
            break;
        case 'feedback':
            sendFeedback(chatId);
            break;
		case '1_____':
            bot.sendMessage(chatId, `________- пробуйте, устанавливайте рекорды и получайте призы. `, option);
            break;
        case '2_____':
            bot.sendMessage(chatId, `________?`, option);
            break;
        case '3_____':
            bot.sendMessage(chatId, `________`, option);
            break;
		case '4_____':
            bot.sendMessage(chatId, `________`, option);
            break;
		case '5_____':
            bot.sendMessage(chatId, `________`, option);
            break;
		case '6_____':
            bot.sendMessage(chatId, `🥇________`, option);
            break;
			
			
        default:
            googleSpreadsheet.addVote(username, text, callback);
            bot.editMessageReplyMarkup({inline_keyboard: [[creteButton('Спасибо за ваш голос!', 'ignore')]]}, {
                chat_id: msg.message.chat.id,
                message_id: msg.message.message_id
            });
    }
});



bot.onText(/\/send ([^\s]+) (.+)/, (msg, match) => {
    if (msg.chat.id !== adminChatId) return;
    const username = match[1];
    const resp = match[2].trim();
    googleSpreadsheet.getChatId(username).subscribe(chatId => {
        if (resp.startsWith('$')) {
            let vote = resp.split('$');
            vote.shift();
            let desc = vote.shift();
            let inline_keyboard = [];
            vote.forEach(i => inline_keyboard.push([creteButton(i)]));
            bot.sendMessage(chatId, desc, {reply_markup: {inline_keyboard: inline_keyboard}});
        } else {
            bot.sendMessage(chatId, resp);
        }
    });
});

bot.onText(/\/sendall (.+)/, (msg, match) => {
    if (msg.chat.id !== adminChatId) return;
    let message = match[1].trim();
    googleSpreadsheet.getChats().subscribe(chats => {
        let options, resp;
        if (message.startsWith('$')) {
            let vote = message.split('$');
            vote.shift();
            resp = vote.shift();
            let inline_keyboard = [];
            vote.forEach(i => inline_keyboard.push([creteButton(i)]));
            options = {reply_markup: {inline_keyboard: inline_keyboard}};
        } else {
            resp = message
        }
        chats.forEach(chat => {
            bot.sendMessage(chat.chatId, resp, options);
        });
    });
});

bot.onText(/\/send_([^\s]+) (.+)/, (msg, match) => {
    if (msg.chat.id !== adminChatId) return;
    const direction = match[1];
    const message = match[2].trim();
    googleSpreadsheet.getChats().subscribe(chats => {
        let options, resp;
        if (message.startsWith('$')) {
            let vote = message.split('$');
            vote.shift();
            resp = vote.shift();
            let inline_keyboard = [];
            vote.forEach(i => inline_keyboard.push([creteButton(i)]));
            options = {reply_markup: {inline_keyboard: inline_keyboard}};
        } else {
            resp = message
        }
        chats.filter(c => c.direction && c.direction.startsWith(direction)).forEach(chat => {
            bot.sendMessage(chat.chatId, resp, options);
        });
    });
});

bot.on('photo', (msg) => {
    if (msg.chat.id !== adminChatId && !msg.caption) return;
    if (msg.caption.startsWith('/sendall')) {
        const desc = msg.caption.match(/\/sendall (.+)/);
        googleSpreadsheet.getAllChatsId().subscribe(chatIds => {
            chatIds.forEach(chatId => {
                bot.sendPhoto(chatId, msg.photo[0].file_id, {caption: desc ? desc[1] : null})
            });
        });
    } else if (msg.caption.startsWith('/send')) {
        const username = msg.caption.match(/\/send ([^\s]*)/);
        const desc = msg.caption.match(/\/send [^\s]*\s(.+)/);
        if (username) {
            googleSpreadsheet.getChatId(username[1]).subscribe(chatId => {
                bot.sendPhoto(chatId, msg.photo[0].file_id, {caption: desc ? desc[1] : null})
            });
        }
    }
});

function getUsername(msg) {
    return msg.username || `${msg.first_name}${msg.last_name ? '_' + msg.last_name : ''}`;
}

function creteButton(text, callback) {
    return {text: text, callback_data: callback ? callback : text};
}

function sendName(msg) {
    bot.editMessageReplyMarkup({inline_keyboard: [[creteButton('✅', 'ignore')]]}, {
        chat_id: msg.chat.id,
        message_id: msg.message_id
    });
	
    const name = msg.chat.username || `${msg.chat.first_name}${msg.chat.last_name ? ' ' + msg.chat.last_name : ''}`;
    const text = `🖐Для того чтобы принимать участие в розыгрыше и общаться с организаторами - давай знакомиться!`;

    const inline_keyboard = {inline_keyboard: [[creteButton('напишите, пожалуйста, Ваше имя', 'edit_name')]]};
    const options = {parse_mode: 'HTML', reply_markup: inline_keyboard};
    bot.sendMessage(msg.chat.id, text, options);
}

function nameReply(msg) {
    bot.editMessageReplyMarkup({inline_keyboard: [[creteButton('✅1', 'ignore')]]}, {
        chat_id: msg.chat.id,
        message_id: msg.message_id
    });
    const name = msg.chat.username || `${msg.chat.first_name}${msg.chat.last_name ? ' ' + msg.chat.last_name : ''}`;
    bot.sendMessage(msg.chat.id, `〰Отлично, ${name}〰`, menuBtnOption());
    sendMenu(msg.chat.id);
}

function nameReply2(msg) {
    bot.editMessageReplyMarkup({inline_keyboard: [[creteButton('✅2', 'ignore')]]}, {
        chat_id: msg.chat.id,
        message_id: msg.message_id
    });
	

    bot.sendMessage(msg.chat.id, NAME_TEXT, ({reply_markup: {force_reply: true}}));
}

function sendQuestion(msg) {
    const text = `🤖Здесь Вы можете задать любой вопрос о мероприятии, а я передам его Support Team!`;
	bot.sendMessage( msg.chat.id, text, {reply_markup: {force_reply: true}});
}

	let ArrSurnameCompare = [];
	 googleSpreadsheet.findNameSurnameInCompareUsersList().subscribe( nameSurnameInCompareUsersList => {
		
			ArrSurnameCompare.push(nameSurnameInCompareUsersList[0] );		
		} ) ;
		

function sendQuestionReceived(chatId, text) {
	
	console.log( chatId + '  chatId | adminChatId '+ adminChatId);
									
									googleSpreadsheet.findNameInConnections().subscribe( nameInConnections => {
												if (nameInConnections[0] == chatId  ) {
																		 if ( nameInConnections[5] == ''  ) {
																						bot.sendMessage(chatId, NAME_TEXT, ({reply_markup: {force_reply: true}}));
																				
																		}else  if ( nameInConnections[5] !== '' ){
																			const msg = `📩 Ваше сообщение:

																			“${text}”

																			Отправлено ✅`;
																			bot.sendMessage( chatId, msg, menuBtnOption());
																		}
	
												}

								   } ) ;
					 
								 googleSpreadsheet.findNameSurnameInCompareUsersList().subscribe( compareUsersList => {

									 if (nospace(compareUsersList[0]) == nospace(text) ) {
										let correctNameSurnameArr = compareUsersList[0];

										 console.log(compareUsersList[1] + ' group  ' );
										 googleSpreadsheet.choiceDirection(chatId, compareUsersList[1]);
										
										bot.sendMessage(chatId, `Приятно познакомиться <strong>${compareUsersList[0]}</strong>`, option);
										bot.sendMessage(chatId, `Вы в комманде <strong>${compareUsersList[1]}</strong>`, option);
										googleSpreadsheet.choiceName(chatId, compareUsersList[0] );
										sendMenu(chatId);
										bot.sendMessage(adminChatId, 'Зарегистрирован - ' + compareUsersList[0] + ' в группе - ' + compareUsersList[1] + ' !  /send_'+ compareUsersList[1]  );

										console.log( countNameSurnameArr+  '  status !!! ' );

									}

								} ) ;

}

function sendFeedback(chatId) {
   bot.sendMessage(chatId, FEEDBACK_TEXT, {reply_markup: {force_reply: true}});
}


function sendDirection(msg) {
    const text = `🎯 В течении вечера для Вас будут доступны интерактивные развлечения. Участвуйте и получайте призы от партнеров за лучшие результаты. `;
    const inline_keyboard = {inline_keyboard: [[creteButton('_____', '1_____')], [creteButton('_____', '2_____')], [creteButton('_____', '3_____')], [creteButton('_____', '4_____')], [creteButton('_____', '5_____')]	]};
    const options = {reply_markup: inline_keyboard};
    bot.sendMessage(msg.chat.id, text, options);
}

function sendDirectionReceived(chatId, msgId, direction) {
    bot.editMessageReplyMarkup({inline_keyboard: [
        [creteButton(`${direction === 'main_direction' ? '✔️' : ''}🔴 _____`, '1_____')],
        [creteButton(`${direction === 'tech_direction' ? '✔️' : ''}🔴 _____`, '2_____')],
        [creteButton(`${direction === 'workshop_direction' ? '✔️' : ''}🔴 _____`, '3_____')],
		[creteButton(`${direction === 'workshop_direction' ? '✔️' : ''}🔴 _____`, '4_____')],
		[creteButton(`${direction === 'workshop_direction' ? '✔️' : ''}🔴 _____`, '5_____')],
		
		]}, {
        chat_id: chatId,
        message_id: msgId
    });
	
	    switch(direction){
        case '1_____':
            direction = '_____';
            break;
        case '2_____':
            direction = '_____';
            break;
        case '3_____':
            direction = '_____';
            break;
		case '4_____':
            direction = '_____';
            break;
		case '5_____':
            direction = '_____';
            break;
    }

}

function sendMenu(chatId) {
    const text = `💭 Чем я могу Вам помочь?`;
    const inline_keyboard = {inline_keyboard: [ [creteButton('🚀  Развлечения', 'direction')], [creteButton('❓ Задать вопрос', 'question')], [creteButton('📮 Оставить отзыв', 'feedback')],  [creteButton('🎁 Призовой фонд', 'winFond')]  ]};
    const options = {reply_markup: inline_keyboard};
	
    setTimeout(() => {
        bot.sendMessage(chatId, text, options);
    }, 500)
	
	 
}

function sendMenuBackBtnOption(chatId) {
	       
    const text = `💭 Чем я могу Вам помочь?`;
    const inline_keyboard = {inline_keyboard: [[creteButton('_____', '1_____')], [creteButton('_____', '2_____')], [creteButton('_____', '3_____')], [creteButton('_____', '4_____')], [creteButton('_____', '5_____')]	]};
    const options = {reply_markup: inline_keyboard};
	
	setTimeout(() => {
        bot.sendMessage(chatId, text, options);
    }, 500)
	
}


function menuBtnOption() {
	   return {reply_markup: {
        keyboard: [['Меню']],
		resize_keyboard: true
    }};

}


